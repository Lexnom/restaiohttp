from dateutil import parser
from aiohttp import web
from model import Tasking
import json
import datetime

async def get_Task(request):
    data = await request.json()
    task = None
    query = None
    if 'id' in data:
        task_id = data['id']
        task = await Tasking.get(task_id)

    if 'overdue' in data:
        task_overdue = data['overdue']
        task = await Tasking.query.where((Tasking.is_done == bool(task_overdue))
                                         and (Tasking.due_date<str(datetime.date.today()))).gino.all()

    if 'created_at_start' in data and 'created_at_end' in data:
        created_at_start = data['created_at_start']
        created_at_end = data['created_at_end']
        task = await Tasking.query.where((Tasking.created_at >= created_at_start)
                                         and (Tasking.created_at <= created_at_end)).gino.all()

    if 'due_date_start' in data and 'due_date_end' in data:
        due_date_start = data['due_date_start']
        due_date_end = data['due_date_end']
        task = await Tasking.query.where((Tasking.due_date >= due_date_start) and
                                         (Tasking.due_date <= due_date_end)).gino.all()

    if 'created_at_start' in data and 'due_date_end' in data:
        created_at_start = data['created_at_start']
        due_date_end = data['due_date_end']
        task = await Tasking.query.where((Tasking.created_at == created_at_start) and
                                         (Tasking.due_date == due_date_end)).gino.all()

    if 'is_done' in data:
        if 'due_date_start' in data and 'due_date_end' in data:
            due_date_start = data['due_date_start']
            due_date_end = data['due_date_end']
            task_is_done = data['is_done']
            task = await Tasking.query.where((Tasking.due_date >= due_date_start) and
                                             (Tasking.due_date <= due_date_end) and
                                             (Tasking.is_done == bool(task_is_done))).gino.all()

        if 'created_at_start' in data and 'created_at_end' in data:
            created_at_start = data['created_at_start']
            created_at_end = data['created_at_end']
            task_is_done = data['is_done']
            task = await Tasking.query.where((Tasking.created_at >= created_at_start) and
                                             (Tasking.created_at <= created_at_end) and
                                             (Tasking.is_done == bool(task_is_done))).gino.all()

        if 'created_at_start' in data and 'due_date_end' in data:
            created_at_start = data['created_at_start']
            due_date_end = data['due_date_end']
            task_is_done = data['is_done']
            task = await Tasking.query.where((Tasking.created_at == created_at_start) and
                                             (Tasking.due_date == due_date_end) and
                                             (Tasking.is_done == bool(task_is_done))).gino.all()

    for res in task:
        query = {'id': res.id,
             'title': res.title,
             'description': res.description,
             'created_at': str(res.created_at),
             'due_date': str(res.due_date),
             'is_done': res.is_done}
    return web.Response(text=json.dumps(query))


async def post_Task(request):
    data = await request.json()
    task_title = data['title']
    task_description = data['description']
    task_created_at = data['created_at']
    task_due_date = data['due_date']
    task_id_done = data['is_done']
    created_date = parser.parse(task_created_at)
    due_date = parser.parse(task_due_date)
    task = await Tasking.create(title=task_title,
                                description=task_description,
                                created_at=created_date,
                                due_date=due_date,
                                is_done=task_id_done)
    query = {'id': task.id,
             'title': task.title,
             'description': task.description,
             'created_at': str(task.created_at),
             'due_date': str(task.due_date),
             'is_done': task.is_done}
    return web.Response(text=json.dumps(query))

async def update_task(request):
    data = await request.json()
    task_id = data['id']
    task_title = data['title']
    task_description = data['description']
    task_due_date = data['due_date']
    task_id_done = bool(data['is_done'])
    due_date = parser.parse(task_due_date)
    task = await Tasking.get(task_id)
    await task.update(title=task_title,
                      description=task_description,
                      due_date=due_date,
                      is_done=task_id_done).apply()
    query = {'id': task.id,
             'title': task.title,
             'description': task.description,
             'created_at': str(task.created_at),
             'due_date': str(task.due_date),
             'is_done': task.is_done}
    return web.Response(text=json.dumps(query))

async def delete_task(request):
    data = await request.json()
    task_id = data['id']
    task = await Tasking.get(task_id)
    await task.delete()
    return web.Response(text=json.dumps({}))

app = web.Application()
app.router.add_get('/api/v1/task/', get_Task)

app.router.add_post('/api/v1/task/', post_Task)

app.router.add_delete('/api/v1/task/', delete_task)

app.router.add_patch('/api/v1/task/', update_task)


if __name__ == '__main__':
    web.run_app(app, host='localhost', port=8080)