from gino.ext.aiohttp import Gino
from config import urlsql
import asyncio, sqlalchemy


db = Gino()
class Tasking(db.Model):
    __tablename__ = 'tasking'

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String)
    description = db.Column(db.String)
    created_at = db.Column(db.Date)
    due_date = db.Column(db.Date)
    is_done = db.Column(db.Boolean)

    def __init__(self, *args, **kwargs):
        super(Tasking, self).__init__(*args, **kwargs)

async def main():
    e = await sqlalchemy.create_engine(urlsql,strategy='gino')
    db.bind = e


asyncio.get_event_loop().run_until_complete(main())